# Create your  User-views here.

from django.contrib.auth import authenticate, login as django_login, logout as django_logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
from django.views import View

# Forms
from .forms import UserLoginForm, UserRegisterForm
from .forms import UserUpdateForm, ProfileUpdateForm
# Messages
from django.contrib import messages

from django.contrib.auth.models import User

# i18n
from django.utils.translation import ugettext as _

def login_view(request):
    """
    Gestiona el login del Usuario
    :param request: objeto HttpRequest con los datos de la petición
    :return: objeto Httpresponse con los datos de la respuesta
    """
    next = request.GET.get('next')
    title = 'Login'
    form = UserLoginForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        django_login(request, user)
        if next:
            return redirect(next)
        #redirect
        return redirect('post:url_post_list')
    context = {
        'form': form,
        'title': title
    }
    return render(request, 'users/login.html', context)


def register_view(request):
    """
    Gestiona el registro del usuario
    :param request: objeto HttpRequeste con los datos de la petición
    :return: objeto HttpResponse con los datos de la respuesta
    """
    next = request.GET.get('next')
    title = 'Register'
    form = UserRegisterForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        password = form.cleaned_data.get('password')
        user.set_password(password)
        user.save()
        new_user = authenticate(username=user.username, password=password)
        django_login(request, new_user)
        if next:
            return redirect(next)
        # redirect
        return redirect('post:url_post_list')

    context = {
        'form': form,
        'title': title
    }

    return render(request, 'users/register.html', context)


def logout_view(request):
    """
    Gestiona el logout del Usuario
    :param request: objeto HttpRequest con los parametros de la petición
    :return: objeto HttpResponse con los parametros de la respuesta
    """
    django_logout(request)
    return redirect('post:url_post_list')
    #return render(request, 'users/logout.html', {})


@login_required
def update(request):
    if request.method == 'POST':
        user_form = UserUpdateForm(request.POST or None,
                                   instance=request.user)

        profile_form = ProfileUpdateForm(request.POST or None,
                                         request.FILES or None,
                                         instance=request.user.profile)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, _('Profile updated successfully'))
        else:
            messages.error(request, _('Error updating profile'))
    else:
        user_form = UserUpdateForm(instance=request.user)
        profile_form = ProfileUpdateForm(instance=request.user.profile)

    context = {
        'user_form': user_form,
        'profile_form': profile_form
    }

    return render(request, 'users/profile.html', context)

@login_required
def user_list(request):
    users = User.objects.filter(is_active=True)
    context = {
        'section': 'people',
        'users': users,
    }
    return render(request, 'follow/list.html', context)


@login_required
def user_detail(request, username):
    user = get_object_or_404(User, username=username, is_active=True)

    context ={
        'section': 'people',
        'user': user,
    }
    return render(request, 'follow/detail.html', context)
