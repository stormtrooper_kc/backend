# -*- coding: utf-8 -*-
from django.contrib import admin
from users.models import Profile

# Register your models here.


# Registramos el perfil de usuario en el admin de Django
class ProfileAdmin (admin.ModelAdmin):
    list_display = ['user',
                    'bio',
                    'date_of_birth',
                    'photo']

admin.site.register(Profile, ProfileAdmin)
