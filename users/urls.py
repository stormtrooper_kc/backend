# -*- coding:utf-8 -*-
from django.conf.urls import url, include

from users.views import login_view, logout_view, register_view, update, user_list, user_detail
from django.contrib.auth.views import (password_change,
                                       password_change_done,
                                       password_reset_confirm,
                                       password_reset,
                                       password_reset_complete,
                                       password_reset_done)

urlpatterns = [
    # Web URLS
    url(r'^login/', login_view, name='login'),
    url(r'^logout/', logout_view, name='logout'),
    url(r'^register/', register_view, name='register'),
    url(r'^profile/', update, name='profile'),
    # User Profiles
    url(r'^users/', user_list, name='user_list'),
    url(r'^users/(?P<username>[-\w]+)/$', user_detail, name='user_detail'),


    # change password URLs
    url('^', include('django.contrib.auth.urls')),
    #url(r'^password-change/$', password_change, name='password_change'),
    #url(r'^password-change-done/$', password_change_done, name='password_change_done'),
    #url(r'^password_reset/$', password_reset, name='password_reset'),
    #url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', password_reset_confirm, name='password_reset_confirm'),
    # API URLS
    url(r'^api/users/', include('users.api.urls', namespace='user-api')),
 ]
