from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import authenticate, login as django_login
from django.contrib.auth import get_user_model
from django.db.models import Q

from rest_framework.serializers import (
    ModelSerializer,
    SerializerMethodField,
    ValidationError,
    EmailField,
    CharField
)

# Models
from users.models import Profile

User = get_user_model()


class UserDetailSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'first_name',
            'last_name',

        ]


class UserCreateSerializers(ModelSerializer):
    email = EmailField(label='Email Address')
    email2 = EmailField(label='Confirm Email')

    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'email2',
            'password',
        ]
        extra_kwargs = {
            'password':
                {
                    'write_only': True
                }
        }

    def validate_email(self, value):
        data = self.get_initial()
        email1 = data.get('email2')
        email2 = value
        if email1 != email2:
            raise ValidationError('Emails must match')
        user_qs = User.objects.filter(email=email2)
        if user_qs.exists():
            raise ValidationError('This user has already registered.')

        return value

    def validate_email2(self, value):
        data = self.get_initial()
        email1 = data.get('email')
        email2 = value
        if email1 != email2:
            raise ValidationError('Emails must match')
        return value

    def create(self, validated_data):
        username = validated_data['username']
        email = validated_data['email']
        password = validated_data['password']

        user_obj = User(username=username,
                        email=email)
        user_obj.set_password(password)
        user_obj.save()

        return validated_data


class UserLoginSerializers(ModelSerializer):
    token = CharField(allow_blank=True, read_only=True)
    username = CharField()

    class Meta:
        model = User
        fields = [
            'username',
            'password',
            'token',
        ]
        extra_kwargs = {
            'password':
                {
                    'write_only': True
                }
        }

    def validate(self, data):
        user_obj = None
        username = data.get('username', None)
        password = data.get('password')
        if not username:
            raise ValidationError('A username or email is required')
        user = User.objects.filter(
            Q(username=username)
        ).distinct()
        if user.exists() and user.count() == 1:
            user_obj = user.first()
        else:
            raise ValidationError('This username is not valid.')
        if user_obj:
            if not user_obj.check_password(password):
                raise ValidationError('Incorrect credentials please try again.')
        user = authenticate(username=username, password=password)
        django_login(data, user)
        data['token'] = 'Some random token'
        return data


class LoginUserSerializer(ModelSerializer):

    class Meta:
        model = User
        fields = [
            'username',
            'password',
        ]


class ProfileSerializer(ModelSerializer):
    photo = SerializerMethodField()

    class Meta:
        model = Profile
        fields = [
            'bio',
            'date_of_birth',
            'photo',
        ]

    @staticmethod
    def get_photo(obj):
        try:
            photo = obj.photo.url
        except:
            photo = None
        return photo


class UserProfileDetailSerializer(ModelSerializer):
    profile = ProfileSerializer(required=False)

    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'first_name',
            'last_name',
            'profile',
        ]
