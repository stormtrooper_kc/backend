# -*- coding:utf-8 -*-from django.conf.urls import url, include
from django.conf.urls import url, include

from .views import (UserCreateAPIView,
                    LoginAPIView,
                    LogoutAPIView,
                    ProfileInfoUpdateView,
                    MyProfileDetail,
                    CurrentUserView,
                    UserDetail)

urlpatterns = [
    # Users API URLS
    url(r'^login/$',  LoginAPIView.as_view(), name='login-api'),
    url(r'^logout/$', LogoutAPIView.as_view(), name='logout-api'),
    url(r'^register/$', UserCreateAPIView.as_view(), name='register-api'),
    # update profile
    url(r'^updateprofile/$', ProfileInfoUpdateView.as_view()),
    # upload image
    #url(r'^uploadimage/$', ProfileImageUploadView.as_view()),

    url(r'^myprofile/$', MyProfileDetail.as_view()),
    url(r'^me/$', CurrentUserView.as_view()),
    url(r'^(?P<pk>\d+)/$', UserDetail.as_view()),
]
