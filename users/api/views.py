from django.contrib.auth import get_user_model
from django.http import Http404
from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,)
from rest_framework.mixins import DestroyModelMixin,UpdateModelMixin
from rest_framework.generics import (
    CreateAPIView,
    ListAPIView,
    RetrieveAPIView,
    UpdateAPIView,
    DestroyAPIView,
    RetrieveUpdateAPIView
)

# Django
from django.contrib.auth import authenticate, login as django_login, logout as django_logout
# Rest Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Permissions
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly)

# Permissions to Post and Pagination
from posts.api.pagination import PostLimitOffsetPagination, PostPageNumberPagination

# Models
from users.models import (Profile,)

# Serializer
from .serializers import (
    UserCreateSerializers,
    UserLoginSerializers,
    LoginUserSerializer,
    UserProfileDetailSerializer,
    ProfileSerializer,
)
from users.constants import generic_response
from users.constants import (FailMessages,
                             ResponseKeys)

User = get_user_model()


class UserCreateAPIView(CreateAPIView):
    serializer_class = UserCreateSerializers
    queryset = User.objects.all()
    permission_classes = [AllowAny]


class LoginAPIView(APIView):
    serializer_class = LoginUserSerializer
    permission_classes = [AllowAny]

    def post(self, request):
        """
        Login de api de Ariticulos
        :param request: objeto HttpRequest con los parametros de la petición
        :return: Response con los estados de la respuesta 
        Ejemplo de petición:
        Header:
            {
                "Content-Type": "application/json"
            }
        Data:
            {
                "username": "richardcs",
                "password": "password"
            }
        
        """
        username = request.data.get('username', None)
        password = request.data.get('password', None)

        if username and password:
            user = authenticate(username=username, password=password)
            if user:
                if user.is_active:
                    django_login(request, user)
                    request.session.save()
                    serializer = self.serializer_class(user)
                    context = {
                        ResponseKeys.SESSION_ID: request.session.session_key,
                        ResponseKeys.USER: serializer.data}
                    return Response(
                        context,
                        status=status.HTTP_200_OK)
                else:
                    return Response(
                        generic_response(FailMessages.USER_INACTIVE),
                        status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response(
                    generic_response(FailMessages.INVALID_CREDENTIALS),
                    status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(
                generic_response(FailMessages.INVALID_CREDENTIALS),
                status=status.HTTP_400_BAD_REQUEST)


class LogoutAPIView(APIView):
    permission_classes = [AllowAny]

    def post(self, request):
        """
        Logout de la api pasando la sessionId generado al realizar el Login
        :param request: objeto HttpRequest con los parametros de la petición
        :return: Responde el estado ok cuando el usuario se desloguea
        Ejemplo de la petición:
        
            {
                "Content-Type": "application/json",
                "sessionId": "token"
            }
        """
        django_logout(request)
        context = {'loggedOut': True}
        return Response(context, status=status.HTTP_200_OK)


class CurrentUserView(APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request):
        serializer = UserProfileDetailSerializer(request.user)
        return Response(serializer.data)


class UserDetail(RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserProfileDetailSerializer


class MyProfileDetail(APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get_object(self, pk):
        try:
            return Profile.objects.get(pk=pk)
        except Profile.DoesNotExist:
            raise Http404

    def get(self, request, format=None):
        user = request.user
        profile = user.profile
        serializer = ProfileSerializer(profile)
        return Response(serializer.data)

    def put(self, request, format=None):
        user = request.user
        profile = user.profile
        serializer = ProfileSerializer(profile, data=request.DATA, files=request.FILES)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProfileInfoUpdateView(APIView):
    """
    ProfileInfoUpdateView APIView\n
    Update User Profile Information
    """
    permission_classes = (IsAuthenticated,)
    permission_classes = [AllowAny]

    def put(self, request, format=None):
        user = request.user

        user.first_name = request.data['first_name']
        user.last_name = request.data['last_name']
        profile = user.profile
        if request.data['bio']:
            profile.university = request.data['bio']
        if request.data['date_of_birth'] != "Invalid date":
            profile.birthday = request.data['date_of_birth']
        profile.save()
        user.save()
        return Response(status=status.HTTP_204_NO_CONTENT)