from django.contrib.sitemaps import Sitemap
from .models import Post


class PostSitemap(Sitemap):
    changefreq = 'weekly'
    priority = 0.9

    def items(self):
        """
        Muestras los lista de articulos del blog
        :return: 
        """
        return Post.objects.filter(draft=False)

    def lastmod(self, obj):
        """
        Mustra la ultima vez que se modifico el Objeto
        :param obj: 
        :return: 
        """
        return obj.updated