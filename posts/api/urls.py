from django.conf.urls import url

#API views
from posts.api.views import (PostListAPIView,
                             PostDetailAPIView,
                             PostDeleteAPIView,
                             PostUpdateAPIView,
                             PostCreateAPIView,
                             PostLikeAPIToggleView,
                             PostDraftListAPIView,
                             PostPublishedListAPIView)

urlpatterns = [
    # API post URLs
    url(r'^$', PostListAPIView.as_view(), name='list_api'),
    url(r'^published/$', PostPublishedListAPIView.as_view(), name='list_published_api'),
    url(r'^draft/$', PostDraftListAPIView.as_view(), name='list_draft_api'),
    url(r'^create/$', PostCreateAPIView.as_view(), name='create_api'),
    url(r'^(?P<pk>\d+)/$', PostDetailAPIView.as_view(), name='detail_api'),
    url(r'^(?P<pk>\d+)/update/$', PostUpdateAPIView.as_view(), name='update_api'),
    url(r'^(?P<pk>\d+)/delete/$', PostDeleteAPIView.as_view(), name='delete_api'),
    # API post like URL
    url(r'^(?P<pk>\d+)/like/$', PostLikeAPIToggleView.as_view(), name='post_api_like'),

]
