from django.utils.timesince import timesince
from rest_framework.serializers import (
    ModelSerializer,
    HyperlinkedIdentityField,
    SerializerMethodField)

# Models
from posts.models import Post

# Serializer
from comments.api.serializers import CommentSerializer
from comments.models import Comment

# Serializer User
from users.api.serializers import UserDetailSerializer

#Tags Serializers
from taggit_serializer.serializers import (TagListSerializerField, TaggitSerializer)


class PostCreateSerializer(TaggitSerializer, ModelSerializer):
    tags = TagListSerializerField()

    class Meta:
        model = Post
        fields = [
            'title',
            'content',
            'publish',
            'image',
            'draft',
            'tags',
        ]

post_detail_url = HyperlinkedIdentityField(
    view_name='posts-api:detail_api',
    lookup_field='pk'
)

post_delete_url = HyperlinkedIdentityField(
    view_name='posts-api:delete_api',
    lookup_field='pk'
)


class PostDetailSerializer(ModelSerializer):
    url = post_detail_url
    user = UserDetailSerializer(read_only=True)
    image = SerializerMethodField()
    publish = SerializerMethodField()
    html = SerializerMethodField()
    comments = SerializerMethodField()
    likes = SerializerMethodField()
    tags = TagListSerializerField(read_only=True)

    class Meta:
        model = Post
        fields = [
            'url',
            'id',
            'user',
            'title',
            'slug',
            'content',
            'html',
            'publish',
            'read_time',
            'image',
            'comments',
            'likes',
            'tags',
        ]

    @staticmethod
    def get_image(obj):
        try:
            image = obj.image.url
        except:
            image = None
        return image

    @staticmethod
    def get_publish(obj):
        return timesince(obj.publish)

    @staticmethod
    def get_html(obj):
        return str(obj.get_markdown())

    @staticmethod
    def get_comments(obj):
        comments_qs = Comment.objects.filter_by_instance(obj)
        comments = CommentSerializer(comments_qs, many=True).data
        return comments

    @staticmethod
    def get_likes(obj):
        if obj.likes:
            return obj.likes.count()
        return 0


class PostListSerializer(ModelSerializer):
    url = post_detail_url
    user = UserDetailSerializer(read_only=True)
    tags = TagListSerializerField(read_only=True)
    image = SerializerMethodField()

    class Meta:
        model = Post
        fields = [
            'url',
            'user',
            'title',
            'content',
            'publish',
            'image',
            'tags',
        ]

    @staticmethod
    def get_image(obj):
        try:
            image = obj.image.url
        except:
            image = None
        return image
