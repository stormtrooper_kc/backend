from rest_framework.generics import (
    CreateAPIView,
    ListAPIView,
    RetrieveAPIView,
    UpdateAPIView,
    DestroyAPIView,
    RetrieveUpdateAPIView
)
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from django.shortcuts import get_object_or_404


# Filters
from rest_framework.filters import (
    SearchFilter,
    OrderingFilter)

# Pagination
from rest_framework.pagination import (
    LimitOffsetPagination,
    PageNumberPagination)
from .pagination import PostLimitOffsetPagination, PostPageNumberPagination

# Permissions
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly)
from .permissions import IsOwnerOrReadOnly

# Models
from posts.models import Post
from .serializers import PostListSerializer, PostDetailSerializer, PostCreateSerializer


class PostCreateAPIView(CreateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostCreateSerializer

    def perform_create(self, serializer):
        """
        Gestiana la creación del articulo vinculado al usuario actual.
        :param serializer: objeto serializer de la del Modelos
        :return: Obejto de respuesta json son los datos
        """
        serializer.save(user=self.request.user)


class PostDetailAPIView(RetrieveAPIView):
    queryset = Post.objects.all()
    serializer_class = PostDetailSerializer
    permission_classes = [AllowAny]


class PostUpdateAPIView(RetrieveUpdateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostCreateSerializer
    permission_classes = [IsOwnerOrReadOnly]

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)


class PostDeleteAPIView(DestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = PostDetailSerializer
    permission_classes = [IsOwnerOrReadOnly]


class PostListAPIView(ListAPIView):
    queryset = Post.objects.all()
    serializer_class = PostListSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['title', 'content', 'user__first_name']
    pagination_class = PostPageNumberPagination # PostLimitOffsetPagination
    permission_classes = [AllowAny]


class PostPublishedListAPIView(ListAPIView):
    """
    View to list all user published post.

    * Allow any users are able to access this view.
    """
    serializer_class = PostListSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['title', 'content', 'user__first_name', 'timestamp']
    pagination_class = PostPageNumberPagination  # PostLimitOffsetPagination
    permission_classes = [AllowAny]

    def get_queryset(self):
        data = self.request
        queryset_list = Post.objects.userpostlist(request=data)
        return queryset_list


class PostDraftListAPIView(ListAPIView):
    """
    View to list all user draft post.

    * Only owner users are able to access this view.
    """
    serializer_class = PostListSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['title', 'content', 'user__first_name', 'timestamp']
    pagination_class = PostPageNumberPagination  # PostLimitOffsetPagination
    permission_classes = [IsOwnerOrReadOnly]

    def get_queryset(self):
        data = self.request
        queryset_list = Post.objects.userdraftlist(request=data)
        return queryset_list


class PostLikeAPIToggleView(APIView):
    """
    View to list all users in the system.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    authentication_classes = (authentication.SessionAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None, pk=None):
        """
        """
        obj = get_object_or_404(Post, pk=pk)
        url_ = obj.get_absolute_url()
        user = self.request.user

        updated = False
        liked = False
        if user.is_authenticated():
            if user in obj.likes.all():
                liked = False
                obj.likes.remove(user)
            else:
                liked = True
                obj.likes.add(user)
            updated = True
        data = {
            'updated': updated,
            'like': liked
        }
        return Response(data)