# -*- coding:utf-8 -*-
from django.contrib import admin

# Register your models here.
from .models import Post


class PostAdmin(admin.ModelAdmin):
    list_display = ['title', 'timestamp', 'updated', 'user']
    list_display_links = ['title', 'user']
    list_filter = ['updated', 'timestamp']
    search_fields = ['title', 'content']
    prepopulated_fields = {'slug': ('title',), }

    class Meta:
        model = Post

admin.site.register(Post, PostAdmin)