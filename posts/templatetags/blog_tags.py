from django.db.models import Count
from django import template

from posts.models import Post
from comments.models import Comment
from taggit.models import Tag

register = template.Library()

# Numero de publicaciones totales de Welldone
@register.simple_tag()
def total_post():
    return Post.objects.active().count()


# Ultimas publicaciones de Welldone
@register.inclusion_tag('posts/latest_posts.html')
def show_latest_post(count=5):
    latest_posts = Post.objects.active().order_by('-publish')[:count]

    context = {
        'latest_posts': latest_posts
    }
    return context


# Mostrar ultimos tags
@register.inclusion_tag('posts/list_tags.html')
def show_latest_tags(count=6):
    latest_tags = Tag.objects.all()
    context = {
        'latest_tags': latest_tags
    }
    return context
