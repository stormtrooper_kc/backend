import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve } from "@angular/router";
import { Observable } from "rxjs/Observable";

import { Post } from "../models/post";
import { PostService } from "./post.service";

@Injectable()
export class PostsResolve implements Resolve<Post[]> {

    constructor(private _postService: PostService) { }
    
    resolve(route: ActivatedRouteSnapshot): Observable<Post[]> {
        return route.params["userId"]
            ? this._postService.getUserPosts(+route.params["userId"])
            : route.params["categoryId"]
                ? this._postService.getCategoryPosts(+route.params["categoryId"])
                : route.params["query"]
                    ? this._postService.searchPosts(route.params["query"])
                    : this._postService.getPosts();
    }
}
