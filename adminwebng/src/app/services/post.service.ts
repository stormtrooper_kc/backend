import { Inject, Injectable } from "@angular/core";
import { Http, Response, URLSearchParams, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";

import { BackendUri } from "./settings.service";
import { Category } from "../models/category";
import { Post } from "../models/post";

@Injectable()
export class PostService {

    constructor(
        private _http: Http,
        @Inject(BackendUri) private _backendUri) { }

    getPosts(): Observable<Post[]> {
        let search = new URLSearchParams();
        search.set("_sort", "publicationDate");
        search.set("_order", "DESC");
        search.set("publicationDate_lte", new Date().getTime().toString());
        let options = new RequestOptions();
        options.search = search;
        return this._http
                   .get(`${this._backendUri}/posts`, options)
                   .map((response: Response) => Post.fromJsonToList(response.json()));
    }

    searchPosts(query: string): Observable<Post[]> {
        let search = new URLSearchParams();
        search.set("_sort", "publicationDate");
        search.set("_order", "DESC");
        search.set("publicationDate_lte", new Date().getTime().toString());
        search.set("q", query);
        let options = new RequestOptions();
        options.search = search;
        return this._http
                   .get(`${this._backendUri}/posts`, options)
                   .map((response: Response) => Post.fromJsonToList(response.json()));
    }

    getUserPosts(id: number): Observable<Post[]> {
        let search = new URLSearchParams();
        search.set("author.id", id.toString());
        search.set("_sort", "publicationDate");
        search.set("_order", "DESC");
        search.set("publicationDate_lte", new Date().getTime().toString());
        let options = new RequestOptions();
        options.search = search;
        return this._http
                   .get(`${this._backendUri}/posts`, options)
                   .map((response: Response) => Post.fromJsonToList(response.json()));
    }

    getCategoryPosts(id: number): Observable<Post[]> {
        let search = new URLSearchParams();
        search.set("_sort", "publicationDate");
        search.set("_order", "DESC");
        search.set("publicationDate_lte", new Date().getTime().toString());
        let options = new RequestOptions();
        options.search = search;
        return this._http
                   .get(`${this._backendUri}/posts`, options)
                   .map((response: Response) => {
                       return Post.fromJsonToList(response.json()).filter((post: Post) => {
                           return post.categories.find((category: Category) => category.id === id) !== undefined;
                       });
                   });
    }

    getPostDetails(id: number): Observable<Post> {
        return this._http
                   .get(`${this._backendUri}/posts/${id}`)
                   .map((response: Response) => Post.fromJson(response.json()));
    }

    createPost(post: Post): Observable<Post> {
        return this._http
                   .post(`${this._backendUri}/posts`, post)
                   .map((response: Response) => Post.fromJson(response.json()));
    }

    updatePost(post: Post): Observable<Post> {
        return this._http
                   .put(`${this._backendUri}/posts/${post.id}`, post)
                   .map((response: Response) => Post.fromJson(response.json()));
    }

    updatePostLikes(id: number, likes: number[]): Observable<Post> {
        let body: any = { "likes": likes };
        return this._http
                   .patch(`${this._backendUri}/posts/${id}`, body)
                   .map((response: Response) => Post.fromJson(response.json()));
    }
}
