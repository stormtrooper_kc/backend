import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Subscription } from "rxjs/Subscription";

import { Category } from "../../models/category";
import { Post } from "../../models/post";
import { PostService } from "../../services/post.service";
import { User } from "../../models/user";

@Component({
    templateUrl: "./post-details.component.html",
    styleUrls: ["./post-details.component.css"]
})
export class PostDetailsComponent implements OnDestroy, OnInit {

    post: Post;
    postInEdition: boolean = false;
    private _sessionUser: User = User.defaultUser();
    private _postSubscription: Subscription;

    constructor(
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _postService: PostService) { }

    ngOnInit(): void {
        this._activatedRoute.data.forEach((data: { post: Post }) => this.post = data.post);
        window.scrollTo(0, 0);
    }

    ngOnDestroy(): void {
        this._unsubscribePostUpdate();
    }

    plainTextToHtml(text: string): string {
        return `<p>${text.replace(/\n/gi, "</p><p>")}</p>`;
    }

    goToAuthorPosts(author: User): void {
        this._router.navigate(["/posts/users", author.id]);
    }

    goToCategoryPosts(category: Category): void {
        this._router.navigate(["/posts/categories", category.id]);
    }

    userCanEditPost(): boolean {
        return !this.postInEdition && this.post.author.id === this._sessionUser.id;
    }

    showPostEditionForm(): void {
        this.postInEdition = true;
    }

    getPostToEdit(): Post {
        return Object.assign({}, this.post);
    }

    updatePost(post: Post): void {
        this._unsubscribePostUpdate();
        this._postSubscription = this._postService
                                     .updatePost(post)
                                     .subscribe((updatedPost: Post) => {
                                         this.post = updatedPost;
                                         this.postInEdition = false;
                                     });
    }

    updatePostLikes(likes: number[]): void {
        this._unsubscribePostUpdate();
        this._postSubscription = this._postService
                                     .updatePostLikes(this.post.id, likes)
                                     .subscribe();
    }

    cancelPostEdition(): void {
        this.postInEdition = false;
    }

    private _unsubscribePostUpdate(): void {
        if (this._postSubscription) {
            this._postSubscription.unsubscribe();
        }
    }
}
