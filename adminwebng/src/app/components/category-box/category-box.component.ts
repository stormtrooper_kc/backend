import { Component, EventEmitter, Input, Output } from "@angular/core";

import { Category } from "../../models/category";

@Component({
    selector: "category-box",
    templateUrl: "./category-box.component.html",
    styleUrls: ["./category-box.component.css"]
})
export class CategoryBoxComponent {

    @Input() categories: Category[];
    @Output() categorySelected: EventEmitter<Category> = new EventEmitter();

    emitCategorySelected(category: Category): void {
        this.categorySelected.emit(category);
    }
}
