import { Component, Input } from "@angular/core";
import { Router } from "@angular/router";

import { Post } from "../../models/post";
import { User } from "../../models/user";

@Component({
    selector: "posts-list",
    templateUrl: "./posts-list.component.html"
})
export class PostsListComponent {

    @Input() posts: Post[];

    constructor(private _router: Router) { }

    goToAuthorPosts(author: User): void {
        this._router.navigate(["/posts/users", author.id]);
    }

    goToPostDetails(post: Post): void {
        this._router.navigate(["/posts", post.id]);
    }
}
