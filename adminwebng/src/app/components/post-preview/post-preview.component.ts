import { Component, EventEmitter, Input, Output } from "@angular/core";

import { Post } from "../../models/post";
import { User } from "../../models/user";

@Component({
    selector: "post-preview",
    templateUrl: "./post-preview.component.html",
    styleUrls: ["./post-preview.component.css"]
})
export class PostPreviewComponent {

    @Input() post: Post;
    @Output() authorSelected: EventEmitter<User> = new EventEmitter();
    @Output() postSelected: EventEmitter<Post> = new EventEmitter();

    emitAuthorSelection(author: User): void {
        this.authorSelected.emit(author);
    }

    emitPostSelection(post: Post): void {
        this.postSelected.emit(post);
    }

    plainTextToHtml(text: string): string {
        return `<p>${text.replace(/\n/gi, "</p><p>")}</p>`;
    }
}
