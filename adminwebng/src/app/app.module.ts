import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app.routing";
import { AutoGrowDirective } from "./directives/auto-grow.directive";
import { BackendUriProvider} from "./services/settings.service";
import { CategoryBoxComponent } from "./components/category-box/category-box.component";
import { CategoryService } from "./services/category.service";
import { FromNowPipe } from "./pipes/from-now.pipe";
import { HeaderBarComponent } from "./components/header-bar/header-bar.component";
import { NewStoryComponent } from "./components/new-story/new-story.component";
import { PostDetailsComponent } from "./components/post-details/post-details.component";
import { PostDetailsResolve } from "./services/post-details-resolve.service";
import { PostFormComponent } from "./components/post-form/post-form.component";
import { PostLikesComponent } from "./components/post-likes/post-likes.component";
import { PostPreviewComponent } from "./components/post-preview/post-preview.component";
import { PostsListComponent } from "./components/posts-list/posts-list.component";
import { PostsResolve } from "./services/posts-resolve.service";
import { PostsViewComponent } from "./components/posts-view/posts-view.component";
import { PostService } from "./services/post.service";
import { SearchBoxComponent } from "./components/search-box/search-box.component";
import { TestComponent } from './test/test.component';

@NgModule({
    imports: [
        AppRoutingModule,
        BrowserModule,
        FormsModule,
        HttpModule,
        BrowserAnimationsModule
    ],
    declarations: [
        AppComponent,
        AutoGrowDirective,
        CategoryBoxComponent,
        HeaderBarComponent,
        FromNowPipe,
        NewStoryComponent,
        PostDetailsComponent,
        PostFormComponent,
        PostLikesComponent,
        PostPreviewComponent,
        PostsListComponent,
        PostsViewComponent,
        SearchBoxComponent,
        TestComponent
    ],
    providers: [
        BackendUriProvider,
        CategoryService,
        PostDetailsResolve,
        PostService,
        PostsResolve,
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule { }
