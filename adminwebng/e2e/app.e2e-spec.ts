import { AdminwelldonePage } from './app.po';

describe('adminwelldone App', () => {
  let page: AdminwelldonePage;

  beforeEach(() => {
    page = new AdminwelldonePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
