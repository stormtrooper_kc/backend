##Internacionalización Django

1. Añadir la carpeta locale en el proyecto principal además,  añadimos el **LOCALE_PATHS** en el **settings.py**.

 ```python
LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'wambo/locale'),
)

```

2. Añadir en el Setting de Django el MIDDLEWARE de locale

``
'django.middleware.locale.LocaleMiddleware',
``

3. Añadimos la traducción  necesaria en las apps del proyecto:
 
 ```python
from django.utils.translation import ugettext as _

message = _('string_translate_example')

```
  
  
4. Crear o actualizar los archivos de internacionalización.

``
python manege.py mekemessages -l <CodigoIdiomatraduccion>
``

Codigos de los idiomas oficiales:

[CodigoIdiomatraduccion](http://www.i18nguy.com/unicode/language-identifiers.html)

5. Una vez generados los ficheros .po se escribe la tradución correspndiente en el **msgstr=**


6. Al competar la traducción compilamos los messages mediante el comando:

``
python manege.py compilemessages
``
Cada vez que cambiamos textos en nuestra applicación debemos realizar el paso 4, 5 y 6. Si no compilamos las traduciones 
no funcionan.

Recomendación no deberíamos versionar los ficheros **.mo** compilados ya que podemos generarlos.