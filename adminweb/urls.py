from django.conf.urls import url

# Import views-post
from adminweb.views import main_view

urlpatterns = [
    # Web post URLs
    url(r'^.*$', main_view, name='url_main_view'),
]
