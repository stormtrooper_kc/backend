from django.shortcuts import render

# Create your views here.

def main_view(request):
    """
    Gestiona el render del admin
    """
    context = {}
    return render(request, 'adminweb/main.html', context)